#!/bin/bash

#set -x
set -e

if [ "$(whoami)" == "root" ]; then
   echo "this program need to be run as user. (do not use sudo) "
   exit 1
fi


sudo sed -i '/^deb cdrom:/ s/^/# /' /etc/apt/sources.list   || echo "problem on sed apt-cdrom"  

sudo apt -q update


# install packages
sudo apt install ansible python3-psutil python3-lxml git make -y         || echo "problem on apt install"


ansible-galaxy collection install community.libvirt
#ansible-galaxy collection install ansible.posix
